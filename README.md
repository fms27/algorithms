# Algorithms

This code accompanies the
Algorithms lecture course for computer science at Cambridge University, taught by [Damon Wischik](https://www.cl.cam.ac.uk/~djw1005/).

* [Algorithms lecture course](https://www.cl.cam.ac.uk/teaching/2021/Algorithms/materials.html)
* [Printed lecture notes](https://www.cl.cam.ac.uk/teaching/2021/Algorithms/notes2.pdf)
* [Videos on YouTube](https://www.youtube.com/playlist?list=PLknxdt7zG11PZjBJzNDpO-whv9x3c6-1H)
