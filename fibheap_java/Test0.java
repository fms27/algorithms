/*
In this test, we're storing objects of class V in a Fibonacci heap.
V is just a dummy test class -- all that a V object really has is an id and a key.

But to be able to put V objects into the heap, we need to let them also store
some Fibonacci-heap data, and we do this by making each V object _use_ a FibHeap.Node
object.
*/

public class Test0 {

    static private class V implements FibHeap.Nodeable<V> {
        String id;
        double key;
        @Override
        public String toString() { return this.id; }

        V(String id, double key) { this.id=id; this.key=key; this.pqn = new FibHeap.Node<Test0.V>(); }

        FibHeap.Node<V> pqn;
        public FibHeap.Node<Test0.V> getFibNode() { return this.pqn; }
        public double key() { return this.key; }
    }

    public static void main(String[] args) {
        test1();
        test2();
    }

    static void test1() {
        V a = new V("a",0.0);
        V b = new V("b",1.0);
        V c = new V("c",2.0);
        V d = new V("d",3.0);
        V e = new V("e",4.0);
        V f = new V("f",5.0);
        V g = new V("g",6.0);
        V h = new V("h",7.0);
        V i = new V("i",8.0);
        V j = new V("j",2.0);

        FibHeap<V> heap = new FibHeap<V>();
        V[] nodes = {a,b,c,d,e,f,g,h,i};
        for (V n : nodes) heap.push(n);
        System.out.println(heap);
    	System.out.println("pop " + heap.popmin());
    	System.out.println(heap);
    	f.key = 0.0; heap.decreaseKey(f);
    	h.key = 5.0; heap.decreaseKey(h);
    	System.out.println(heap);
    	e.key = 1; heap.decreaseKey(e);
    	g.key = 0; heap.decreaseKey(g);
    	heap.push(j);
    	System.out.println(heap);
    	System.out.println("pop " + heap.popmin());
    	System.out.println(heap);
    	i.key = -1;
    	heap.decreaseKey(i);
    	h.key = -1;
    	heap.decreaseKey(h);
    	System.out.println("pop " + heap.popmin());
    	System.out.println("pop " + heap.popmin());
    	System.out.println(heap);
	}

    static void test2() {
        V a = new V("a",1.0);
        V b = new V("b",2.0);
        V c = new V("c",3.0);
        FibHeap<V> heap = new FibHeap<V>();
        heap.push(a); heap.push(b); heap.push(c);
        System.out.println(heap);
        System.out.println("decreasekey(c)");
        c.key = 0.0; heap.decreaseKey(c);
        System.out.println(heap);
        System.out.println("pop " + heap.popmin());
        System.out.println(heap);
    }
}
