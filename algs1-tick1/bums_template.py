#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# bums.py
# (c) Frank Stajano 2020 -- 2022
# $Id: bums_template.py 11 2022-12-14 12:25:54Z fms27 $

# Copyright (c) <year>, <student author of the missing bits>

# This software was produced as an exercise for the Algorithms 1
# course at the University of Cambridge. Redistribution facilitates
# plagiarism and is not allowed. Reuse by the course lecturer is allowed.


"""Sort an n-item array using bottom-up mergesort. You are only given
enough scratch space to hold half of the original array (n/2 rounded
down).

The strategy must be to move the rightmost chunk into scratch space,
because the rightmost chunk, even at the top level, can never be
larger than half the array (whereas the leftmost chunk can: 9 cells
will split as 8+1 at the top level).

"""


# pylint: disable=invalid-name, misplaced-comparison-constant

import math
import nrsa
import selftest


class Sorter:
    """Wrapper class for bottom-up merge sort.

    An object of this class is initialised with the data to be sorted
    and the scratch space to be used for sorting. Sort the data by
    invoking the sort() method.
"""

    @staticmethod
    def _desiredScratchSize(n):
        """Return the desired size of scratch space to deal with an array of
        size n, namely n/2 rounded down.

        PRECONDITION:
        n is a non-negative integer.

        """
        assert isinstance(n, type(0))
        assert n >= 0
        result = int(math.floor(n/2.0))
        return result

    def __init__(self, dataArray, scratchArray):
        assert scratchArray.size() == Sorter._desiredScratchSize(
            dataArray.size())
        self.d = dataArray
        self.s = scratchArray

    def passes(self):
        """Return an integer denoting the number of passes that need to be
        made on the array in order to mergesort it bottom-up. Pass 0 merges
        chunks of size up to 2^0; in general, pass i merges chunks of size up
        to 2^i. If the size of the data array is 8, 3 passes are required (to
        merge chunks of size 1, 2 and 4). For size 9, 4 passes would be
        required."""

        # XXX implement this

    def chunkSizeInPass(self, p):
        """Return an integer denoting the (maximum) size of a chunk during
        pass p, where passes are numbered from 0 as described in the
        docstring for the passes() method.

        PRECONDITION: p is an integer between 0 included and
        self.passes() excluded.
        """
        assert isinstance(p, type(0))
        assert p >= 0
        assert p < self.passes()

        # XXX implement this

    @staticmethod
    def lddr(arraySrc, iStartSrc, iEndSrc, arrayDst, iStartDst, iEndDst):
        """Copy the source block arraySrc[iStartSrc:iEndSrc] to the
        destination area arrayDst[iStartDst:iEndDst]. The blocks must
        be of the same size. The blocks may or may not be in the same
        array. If they are in the same array, they may be identical
        (in which case this function call is a no-operation) or they
        may overlap provided that iEndDst > iEndSrc. The name of this
        method is inspired by the Z80 assembly language block copy
        instruction LDDR, which stood for "load, decrement, repeat".

        PRECONDITION:

        0) arraySrc and arrayDst are nrsa.NonResizableStringArray,
        whereas the i parameters are integer indices into them.

        1) source and destination regions have same size.

        2) either they are the same, they don't overlap or the
        source comes first.

        POSTCONDITION:

        The destination region contains the same values that the
        source region did, in the same order. If the regions didn't
        overlap, the source region is unchanged.

        """

        # Preconditions
        assert isinstance(arraySrc, nrsa.NonResizableStringArray)
        assert isinstance(arrayDst, nrsa.NonResizableStringArray)
        assert isinstance(iStartSrc, type(0))
        assert isinstance(iEndSrc, type(0))
        assert isinstance(iStartDst, type(0))
        assert isinstance(iEndDst, type(0))
        assert iEndSrc - iStartSrc == iEndDst - iStartDst  # same size
        assert (  # note that these are "or else" in Python
            (arraySrc != arrayDst)  # non-overlapping
            or (iStartSrc == iStartDst)  # identical, since same size
            or (iEndSrc <= iStartDst)   # non-overlapping
            or (iEndDst <= iStartSrc)  # non-overlapping
            or (iStartSrc < iStartDst)  # overlapping but not overwriting
            )

        # XXX implement this

    def mergeRL(
            self,
            arraySrc1, iStartSrc1, iEndSrc1,  # src1
            arraySrc2, iStartSrc2, iEndSrc2,  # src2
            arrayDst, iStartDst, iEndDst,  # dst
            ):
        """Merge the array regions arraySrc1[iStartSrc1:iEndSrc1] and
        arraySrc2[iStartSrc2:iEndSrc2], putting the result in
        arrayDst[iStartDst:iEndDst] (NB we use python slice notation,
        left end included and right end excluded, even though it does
        not work on the homemade arrays from the supplied nrsa
        module.) Proceed right-to-left in all three arrays.

        PRECONDITION:
        1) The source regions
        arraySrc1[iStartSrc1:iEndSrc1] and
        arraySrc2[iStartSrc2:iEndSrc2] are sorted in ascending order.

        2) The sum of the sizes of the two sources is equal to the
        size of the destination.

        3) The caller guarantees that proceeding right to left in all
        three arrays will not overwrite any of the source values.

        POSTCONDITION: the destination region
        arrayDst[iStartDst:iEndDst] is sorted in ascending order.

        """

        # Preconditions
        # 1)
        for i in range(iStartSrc1, iEndSrc1-1):
            assert arraySrc1.read(i) <= arraySrc1.read(i+1), (
                arraySrc1.read(i), arraySrc1.read(i+1))
        for i in range(iStartSrc2, iEndSrc2-1):
            assert arraySrc2.read(i) <= arraySrc2.read(i+1), (
                arraySrc2.read(i), arraySrc2.read(i+1))
        # 2)
        assert iEndSrc1-iStartSrc1+iEndSrc2-iStartSrc2 == iEndDst-iStartDst, (
            iStartSrc1, iEndSrc1, iStartSrc2, iEndSrc2, iStartDst, iEndDst)
        # XXX check precondition 3 yourself if you like (you don't have to)

        # XXX implement this

    def sort(self):
        """Sort the values in self.d in ascending order, leaving them in
        self.d, using the bottom-up merge sort algorithm, without
        using any sorting functions from Python or its library, using
        self.s as scratch space and without allocating any further memory to
        hold values from self.d.

        PRECONDITION: self.d is an nrsa array of strings. self.s is
        another, which can be overwritten, which holds as much space
        as necessary (see module docstring---to avoid inconsistencies
        in natural language descriptions I don't want to repeat the
        condition in several comments).

        POSTCONDITON self.d contains the same values as before, but
        sorted in ascending order.

        """

        # XXX implement this


        
