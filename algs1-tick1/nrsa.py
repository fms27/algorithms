#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# nrsa.py
# (c) Frank Stajano 2022-10-20 -- 2022-11-27
# $Id: nrsa.py 11 2022-12-14 12:25:54Z fms27 $

"""Constrained non-resizable string array class for the algorithms tick."""

# pylint: disable=invalid-name, misplaced-comparison-constant

class NonResizableStringArray:
    """Non-resizable array with basic accessors. The type of the stored
    values is enforced to be string."""

    DEFAULT_VALUE = "UNDEFINED"

    def __init__(self, n):
        assert isinstance(n, type(0))
        assert n >= 0
        self._n = n
        self._a = [NonResizableStringArray.DEFAULT_VALUE, ] * n

    def read(self, index):
        """Return the value stored in the cell at the given index."""
        assert self._isValidIndex(index)
        return self._a[index]

    def write(self, index, value):
        """Write the supplied value into the cell at the given index."""
        assert self._isValidIndex(index)
        assert self._isValidValue(value)
        self._a[index] = value

    def size(self):
        """Return the number of cells in the array."""
        return self._n

    def _isValidIndex(self, index):
        assert isinstance(index, type(0))
        assert index >= 0
        assert index < self._n, (index, self._n, self._a)
        return True

    def _isValidValue(self, value):
        assert isinstance(value, type(self.DEFAULT_VALUE))
        return True
